# -*- coding: utf-8 -*-
#
#	Copyright 2013 The Binary Workshop.
#	
__doc__ = """\
sshxy.py - A plugin to integrate GateOne with SSHxy application.

Hooks
-----
This Python plugin file implements the following hooks::

	hooks = {		
   	'WebSocket':{
      	'terminal:sshxy_session_sync':session_sync,
      	'terminal:sshxy_identity_get':identity_get,
      	'terminal:sshxy_terminal_exit':terminal_exit,
      	'terminal:sshxy_session_playback':session_playback,
      	'terminal:sshxy_session_destroy':session_destroy
   	}
	}

Docstrings
----------
"""

# Meta information about this python plugin.
__version__ = '1.1'
__version_info__ = (1,1)
__license__ = "AGPLv3"
__author__ = 'John Sundarraj <sundarraj@thebw.in>'

# Importing python modules.
import os,logging
from gateone.core.locale import get_translation

# Get Translation.
#_ = get_translation()

# Path to our plugin directory.
PLUGIN_PATH = os.path.split(__file__)[0]

def session_sync(self,vars):
	"""
	When *terminal:new_terminal* event occurs, :func:`GateOne.SSHxy._sessionSync` gets executed
	on the client end, which triggers 'terminal:sshxy_session_sync' hook	via websocket.	
	This function triggers :func:`GateOne.SSHxy.sessionSync` on client end via websocket by passing 
	*params* as arguments.
	"""
	try:
		import signal
		from gateone.core.utils import get_process_tree
		session = self.ws.session
		self.ws.locations[self.ws.location]['terminal'] = {}
		stale_process = []
		for f in os.listdir('/proc'):
			pid_dir = os.path.join('/proc',f)
			if os.path.isdir(pid_dir):
				try:
					pid = int(f)
				except ValueError:
					continue
				try:
					with open(os.path.join(pid_dir,'cmdline')) as f:
						cmdline = f.read()
					if cmdline and session in cmdline:
						if 'ssh:' in cmdline:						
							stale_process.append(pid)
				except Exception as error:
					pass	
		for pid in stale_process:
			kill_process = get_process_tree(pid)
			for _pid in kill_process:
				_pid = int(_pid)
				try:
					os.kill(_pid,signal.SIGTERM)			
				except OSError:
					pass
		params = {
			'user':self.current_user['upn'],
			'session':session,			
			'status':'success'
		}
	except Exception as error:
		params = {'status':'failure'}
	data = {'terminal:sshxyjs_session_sync':params}
	self.write_message(data)

def identity_get(self,vars):
	"""
	:func:`GateOne.SSHxy._sessionSync` triggers 'terminal:sshxy_identity_get' hook via
	websocket, which in turn get the identity data from SSHxy frontend. This function is
	the last step of initialization process, which triggers :func:`GateOne.SSHxy.identityGet`
	on client end via websocket by passing *params* as arguments.
	"""
	try:
		import urllib2,json
		user = self.current_user['upn']
		ssh_dir = os.path.join(os.path.join(self.settings['user_dir'],user),'.ssh')
		request = urllib2.Request(vars['url'])
		request.add_header('Accept','application/json')
		output = urllib2.urlopen(request)
		output = json.loads(output.read())
		key = output['private_key']
		key = key.replace('\r\n','\n')
		key_path = os.path.join(ssh_dir,vars['file'])
		if not os.path.exists(key_path):
			with open(key_path,'w') as f:
				f.write(key)
			os.chmod(key_path,0600)
		else:
			pass
		params = {
			'user':vars['user'],
			'key':vars['file'],
			'ssh_dir':ssh_dir,	
			'status':'success'
		}
	except Exception as error:
		params = {'status':'failure'}
	data = {'terminal:sshxyjs_identity_get':params}
	self.write_message(data)

def terminal_exit(self,vars):
	"""
	Closing live terminal triggers *terminal:term_ended* hook via websocket, which executes
	:func:`GateOne.SSHxy._terminalExit`. This in turn triggers 'terminal:sshxy_terminal_exit'
	hook via websocket, which triggers :func:`GateOne.SSHxy.terminalExit` on client end via 
	websocket by passing *params* as arguments.
	"""
	try:
		user = self.current_user['upn']
		ssh_dir = os.path.join(os.path.join(self.settings['user_dir'],user),'.ssh')
		key = os.path.join(ssh_dir,vars['file'])
		known_hosts = os.path.join(ssh_dir,'known_hosts')
		if os.path.exists(key):
			os.remove(key)
		if os.path.exists(known_hosts):
			os.remove(known_hosts)		
		params = {
			'user':user,
			'key':vars['file'],
			'session':self.ws.session,	
			'status':'success'
		}		
	except Exception as error:
		params = {'status':'failure'}
	data = {'terminal:sshxyjs_terminal_exit':params}
	self.write_message(data)

def session_playback(self,vars):
	"""
	:func:`GateOne.SSHxy.terminalExit` executes :func:`GateOne.SSHxy.sessionPlayback` on
	client end which in turn triggers 'terminal:sshxy_session_playback' hook via websocket
	to execute this function and sends the playback to SSHxy frontend.
	"""
	try:
		import urllib2,json,tornado.template
		from playback import get_256_colors
		template_path = os.path.join(PLUGIN_PATH,'templates')
		template_path = os.path.join(template_path,'sshxy-playback.html')
		with open(template_path) as f:
			template_file = f.read()
		template = tornado.template.Template(template_file)
		playback = template.generate(
			recording=vars['recording'],
			container=vars['container'],
			prefix=vars['prefix'],
			theme=vars['theme_styles'],
			colors=vars['color_styles'],
			colors_256=get_256_colors(self)
		)
		playback_path = os.path.join(vars['path'],vars['session']+'.html')
		if not os.path.exists(playback_path):
			with open(playback_path,'w') as f:
				f.write(playback)
			os.chmod(playback_path,0775)
		elif os.path.exists(playback_path):
			try:
				os.remove(playback_path)
				with open(playback_path,'w') as f:
					f.write(playback)
				os.chmod(playback_path,0775)
			except Exception as error:
				pass
		else:
			pass																					
		params = {
			'id':vars['id'],
			'status':'success'
		}
	except Exception as error:
		params = {
			'id':vars['id'],
			'status':'failure'
		}
	request = urllib2.Request(vars['url'])
	request.add_header('Content-Type','application/json;charset=UTF-8')
	request.add_header('Accept','application/json;charset=UTF-8')
	request.add_data(json.dumps(params))
	urllib2.urlopen(request)
	#request.close()

def session_destroy(self,vars):
	"""
	:func:`GateOne.SSHxy.terminalExit` triggers 'terminal:sshxy_session_destroy' via 
	websocket, which in turn executes this function. This function triggers 
	:func:`GateOne.SSHxy.sessionDestroy` on client end via websocket by passing *params* 
	as arguments.
	"""
	try:
		import shutil
		from gateone.applications.terminal.app_terminal import kill_session
		kill_session(vars['session'],True)
		user = self.current_user['upn']
		user_session_dir = os.path.join(self.ws.settings['session_dir'],vars['session'])
		shutil.rmtree(user_session_dir)
		user_session_file = os.path.join(os.path.join(self.settings['user_dir'],user),'session')
		os.remove(user_session_file)
		params = {'status':'success'}
	except Exception as error:
		params = {'status':'failure'}
	data = {'terminal:sshxyjs_session_destroy':params}
	self.write_message(data)

hooks = {
	'WebSocket':{
		'terminal:sshxy_session_sync':session_sync,
		'terminal:sshxy_identity_get':identity_get,
		'terminal:sshxy_terminal_exit':terminal_exit,
		'terminal:sshxy_session_playback':session_playback,
		'terminal:sshxy_session_destroy':session_destroy
	}
}
